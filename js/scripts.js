$(document).ready(function() {
    $(".wp1").waypoint(function() { $(".wp1").addClass("animated fadeInLeft") }, { offset: "75%" });
    $(".wp2").waypoint(function() { $(".wp2").addClass("animated fadeInDown") }, { offset: "75%" });
    $(".wp3").waypoint(function() { $(".wp3").addClass("animated bounceInDown") }, { offset: "75%" });
    $(".wp4").waypoint(function() { $(".wp4").addClass("animated fadeInDown") }, { offset: "75%" });
    $(".wp5").waypoint(function() { $(".wp5").addClass("animated-d1s fadeInLeft") }, { offset: "75%" });
    $(".wp6").waypoint(function() { $(".wp6").addClass("animated-d1s fadeInDown") }, { offset: "75%" });
    $("#quoteSlider").flickity({ cellAlign: "left", contain: true, prevNextButtons: false, autoPlay: true, pauseAutoPlayOnHover: true, wrapAround: true });
    $("#iPhoneSlider").flickity({ cellAlign: "left", contain: true, prevNextButtons: false, imagesLoaded: true, autoPlay: true, pauseAutoPlayOnHover: true, wrapAround: true });
    $(".youtube-media").on("click", function(e) {
        var t = $(window).width();
        if (t <= 768) { return }
        $.fancybox({ href: this.href, padding: 4, type: "iframe", href: this.href.replace(new RegExp("watch\\?v=", "i"), "v/") });
        return false
    });

    // /*==================================================================
    //    [ when select pricing fill message contact form with selected pricing and scroll  ]*/

    $("#btnFree").click(function() {
        var text = "Hello. I am interested in FREE package. Can you give some details, please?";
        document.forms.contactForm.message.value = text;
        $("html,body").animate({ scrollTop: $("#contact").offset().top }, 2e3);
    });
    $("#btnPlus").click(function() {
        var text = "Hello. I am interested in PLUS package. Can you give some details, please?";
        document.forms.contactForm.message.value = text;
        $("html,body").animate({ scrollTop: $("#contact").offset().top }, 2e3);
    });
    $("#btnPro").click(function() {
        var text = "Hello. I am interested in PRO package. Can you give some details, please?";
        document.forms.contactForm.message.value = text;
        $("html,body").animate({ scrollTop: $("#contact").offset().top }, 2e3);
    });

});


// /*==================================================================
//    [ Smooth scroll to selected anchor ]*/

$(document).ready(function() { $("a.single_image").fancybox({ padding: 4 }) });
$(".nav-toggle").click(function() {
    $(this).toggleClass("active");
    $(".overlay-boxify").toggleClass("open")
});
$(".overlay ul li a").click(function() {
    $(".nav-toggle").toggleClass("active");
    $(".overlay-boxify").toggleClass("open")
});
$(".overlay").click(function() {
    $(".nav-toggle").toggleClass("active");
    $(".overlay-boxify").toggleClass("open")
});
$("a[href*=#]:not([href=#])").click(function() {
    if (location.pathname.replace(/^\//, "") === this.pathname.replace(/^\//, "") && location.hostname === this.hostname) {
        var e = $(this.hash);
        e = e.length ? e : $("[name=" + this.hash.slice(1) + "]");
        if (e.length) { $("html,body").animate({ scrollTop: e.offset().top }, 2e3); return false }
    }
})


// /*==================================================================
//    [ Cookie Banner Display on load ]*/

$(window).load(function() {
    window.wpcc.init({
        "corners": "small",
        "colors": {
            "popup": {
                "background": "#f6f6f6",
                "text": "#3F6184",
                "border": "1px solid black"
            },
            "button": {
                "background": "#73d0da",
                "text": "#fff"
            }
        },
        "position": "bottom",
        "content": {
            "href": "privacyPolicy.html",
            "link": "Cookie policy"
        }
    })
});