<?php
require_once './vendor/autoload.php';

try {
    //Get variables
    $firstName = $_POST['first-name'];
    $lastName = $_POST['last-name'];
    $email = $_POST['email'];
    $company = $_POST['company'];
    $phone = $_POST['phone'];
    $msg = $_POST['message'];

    // Create the SMTP transport
    $transport = (new Swift_SmtpTransport('smtp.dianoia-it.com', 587))
        ->setUsername('adrian@dianoia-it.com')
        ->setPassword('19adi@Dia*');

    $mailer = new Swift_Mailer($transport);

    // Create a message
    $message = new Swift_Message();

    $message->setSubject('T-Gate Automatic Mailing System');
    $message->setFrom(['adrian@dianoia-it.com' => 'T-Gate']);
    $message->addTo('adrian.valentin.iuga@gmail.com');

    $message->setBody("A contact form was recived from T-Gate!" . "\n\n" . $firstName . " " . $lastName . "\n" .  $company . "\n" . $email . "\n" . $phone . "\n" . $msg);

    // Send the message
    $result = $mailer->send($message);

    header('Location: https://dianoia-it.com/webs/v3/');
} catch (Exception $e) {
    echo $e->getMessage();
}
